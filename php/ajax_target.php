<?php
try {
    $pdo = new PDO("mysql:dbname=apteka;host=localhost", "root", "", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}catch (PDOException $e) {
    echo "Ошибка:".$e->getMessage();
    exit;
}

$sql_query = $_POST["sql_query"];

$find = $pdo->prepare($sql_query);
$find->execute();
$result = $find->fetchAll();