<?php
include('php/start.php');
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Велес | Аптека</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/polzynok.css" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>

<body>
    <div class="mask-content"></div>
    <div id="menu_mobile">
        <div class="icon-close">
            <i class="fa fa-times fa-2x"></i>
        </div><br>

        <ul>
            <li><a href="index.html">Главная </a></li>
            <li><a href="uslugi.html">Наши услуги </a></li>
            <li><a href="shelter.html">Стационар</a></li>
            <li><img src="images/icon_apteka.png" alt="lapa">
                <a href="apteka.php">Аптека</a></li>
            <li><a href="contacts.html">Контакты</a></li>
            <li><a href="vacancies.html">Вакансии</a></li>
        </ul>
    </div>
    <header>
        <div class="container">
            <div class="row">
                <a href="index.html"><div class="logo">
                    <img src="images/logo.png" alt="Велес">
                    </div></a>
                <div class="phone_number">
                    <p>Беспокоит состояние Вашего питомца?</p><br>
                    <p>Позвоните нам: <span>+38(093)80-35-230</span></p><br>
                    <p><span>+38(096)76-50-959</span> </p>
                </div>
                <div style="clear: right"></div>
                <div class="navbar">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="" data-target="#responsive-menu">
                            <span class="sr-only">Открыть навигацию</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="responsive-menu">
                        <ul class="nav navbar-nav">
                            <li class="first_li "><a href="index.html">Главная </a></li>
                            <li class="second_li "><a href="uslugi.html">Наши услуги </a></li>
                            <li class="third_li "><a href="shelter.html">Стационар</a></li>
                            <li class="third_li active_tab"><img src="images/icon_apteka.png" alt="lapa">
                                <a href="apteka.php">Аптека</a></li>
                            <li class="fifth_li"><a href="contacts.html">Контакты</a></li>
                            <li class="fifth_li "><a href="vacancies.html">Вакансии</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

    </header>

<div id="gridSystemModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true"> <!--Модальное окно-->
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>



    <section id="apteka">
        <div class="container">
            <div class="row">
                <h2>Препараты</h2>
                <select onchange="slctt(this.value)" class="target">
                    <option value="1">По дате добавления</option>
                    <option value="2">От дешевых к дорогим</option>
                    <option value="3">От дорогих к дешевым</option>
                </select>
                <div style="clear: both;"></div>
                <hr class="goods_line">
                <div id="filter">
                    <div id="search_filter">
                        <p>Название препарата:</p>
                        <div>
                            <form method="post" onsubmit="event.preventDefault()" action="do_search.php">
                            <input type="text" name="search" id="search_box" class='search_box'>
                            <div class="search_button">
                                <img src="images/search_icon.png">
                            </div>
                            </form>
                            <button  class="filter_button" onclick="location.reload()">
                                Очистить поиск
                            </button>
                        </div>
                    </div>
                    <div id="group_filter">
                        <p>Группы препаратов:</p>
                        <div class="menu_item catalog" onclick="my_f1()">
                            <p>Для кошек <i id="dropdown_angle1" class="fa fa-angle-up "></i></p>
                        </div>
                        <div class="dropdown_block">
                            <form>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="1">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Антигельминтики для котов</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="2">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Инсектоакарицидные для котов</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="3">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Витамины и минералы для котов</span>
                                </label>
                            </form>
                        </div>
                        <div class="menu_item catalog" onclick="my_f2()">
                            <p>Для собак <i id="dropdown_angle2" class="fa fa-angle-down "></i></p>
                        </div>
                        <div class="dropdown_block drop_none">
                            <form method="post">
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="4">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Антигельминтики для собак</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="5">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Инсектоакарицидные для собак</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="6">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Витамины и минералы для собак</span>
                                </label>
                            </form>
                        </div>
                        <div class="menu_item catalog" onclick="my_f3()">
                            <p>Универсальные <i id="dropdown_angle3" class="fa fa-angle-down "></i></p>
                        </div>
                        <div class="dropdown_block drop_none">
                            <form>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="7">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Гепатопротекторы</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="8">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Капли и мази глазные</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="9">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Капли и мази ушные</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="10">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Контрацептивы</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="11">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Наружные препараты (мази, спреи, гели)</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="12">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Стоматологические препараты</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="13">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Препараты, влияющие на ЦНС</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="14">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Препараты для почек и мочевого пузыря</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="15">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Препараты для почек и мочевого пузыря</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="16">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Препараты для работы сердца</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test" value="17">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Препараты для пищеварительной системы</span>
                                </label>
                                <label>
                                    <input class="checkbox" type="checkbox" name="checkbox-test"  value="18">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Противовоспалительные</span>
                                </label>
                            </form>
                        </div>
                    </div>
                    <div id="price_filter">
                        <p>Цена:</p>
                        <div class="masonry">
                            <div id="slider-range"></div>
                            <form action="" method="post">
                                <p><input name="min_price" type="text" id="amount"> грн</p>
                                <p><input name="max_price" type="text" id="amount_1"> грн</p>
                            </form>
                        </div>
                    </div>
                    <button class="filter_button">Найти</button>
                </div>
                <div id="goods">

                    <?php

                    $i = 0;
                    if (!empty($result)):
                        foreach ($result as $item):
                             $i++;
                    ?>
                    <div class="selector">
                        <div class="good_img">
                            <img src="<?php if(isset($item['photo'])) {
                            print $item['photo'];}
                            else {
                                print "Ошибка";
                            }?>" alt="preparat">
                        </div>
                        <p class="name"><?php if(isset($item['name'])) {
                                print $item['name'];}
                            else {
                                print "Ошибка";
                            }?></p><br>
                        <p class="price"><?php if(isset($item['price'])) {
                                print $item['price'];}
                            else {
                                print "Ошибка";
                            }?> грн</p>
                        <button class="button" data-toggle="modal" data-target="#gridSystemModal" data-id='<?=$item['id']?>'>Узнать больше</button>
                    </div>
                    <?php endforeach;
                    endif;?>
                <div id="pagination">
                    <div id="page_panel"></div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <div id="log"></div>
   <footer class="container">
        <div class="row">
            <hr>
            <p>Ветеренарная клиника «Велес» &nbsp; | &nbsp; пгт. Макаров,  Киевская обл, ул.Дмитрия Ростовского 20б     &nbsp; | &nbsp; Тел.  +38(093)80-35-230 &nbsp; | &nbsp; <a href="contacts.html">Контакты</a> </p>
            <p>Copyright © 2017 &nbsp; | &nbsp; All rights reserved</p>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <script src="js/bootstrap.js" type="text/javascript"></script>

<script>

        $(document).ready(function() {
            pagination();
        });
        function pagination() {
            Panel(
                {
                    inrow: 3,
                    items: <?=$number_pages?>,
                    step: 3,
                    current: 1,
                    callback: clickPanel,
                    element: document.getElementById('page_panel')
                }
            );
        };

        function clickPanel(page) {
            $("#goods").html("Loading...");
            var searchString    = $("#search_box").val();
            var page1;
            var data = {
                min_price: $('[name="min_price"]').val(),
                max_price: $('[name="max_price"]').val(),
                category_list: category_list,
                sorting_value: sorting_value,
                page: page,
                search: searchString,
                number_pages: <?=$number_pages?>,
                number_items: <?=$number_items?>
            };
            $.ajax({
                type: "POST",
                data: data,
                url: "php/ajax_goods.php",
                success: function(html){
                    $("#goods").html(html);
                }
            });
            page1 = page;
        }

        (function () {
            function Panel(params) {
                if (!(this instanceof Panel)) {
                    return new Panel(params);
                }
                this.initialize.apply(this, arguments);
            }

            Panel.prototype.initialize = function (arg) {
                var fragment = document.createDocumentFragment();
                var first = document.createElement('span');
                var ul = document.createElement('ul');
                var li = document.createElement('li');
                var last = first.cloneNode();
                var prev = first.cloneNode();
                var next = first.cloneNode();
                var callback = arg.callback;
                var element = arg.element;
                var ins_li, end;

                first.className = 'first';
                last.className = 'last';
                prev.className = 'prev';
                next.className = 'next';
                arg.step = arg.step || 1;
                arg.inrow = arg.inrow || 5;
                arg.items = arg.items || 25;
                arg.current = arg.current || 1;
                arg.prev_txt = arg.prev_txt || '<';
                arg.next_txt = arg.next_txt || '>';
                arg.last_txt = arg.last_txt || '>>';
                arg.first_txt = arg.first_txt || '<<';
                var show_button = arg.items > arg.inrow;
                if(show_button){
                    first.appendChild(document.createTextNode(arg.first_txt));
                    last.appendChild(document.createTextNode(arg.last_txt));
                    prev.appendChild(document.createTextNode(arg.prev_txt));
                    next.appendChild(document.createTextNode(arg.next_txt));
                }
                element.className = 'page_panel';
                var start = Math.floor((arg.current - 1) / arg.inrow) * arg.inrow;

                function build(start) {
                    for (end = arg.inrow + start; ++start <= end;) {
                        if (start > arg.items) break;
                        ins_li = li.cloneNode();
                        if (start == arg.current) {
                            ins_li.className = 'active';
                        }
                        ins_li.innerHTML = start;
                        fragment.appendChild(ins_li);
                    }
                    ul.innerHTML = '';
                    ul.appendChild(fragment);
                    if(show_button){
                        fragment.appendChild(first);
                        fragment.appendChild(prev);
                    }
                    fragment.appendChild(ul);
                    if(show_button){
                        fragment.appendChild(next);
                        fragment.appendChild(last);
                    }
                    element.appendChild(fragment);
                }

                build(start);

                element.onclick = function (e) {
                    var el = e ? e.target : window.event.srcElement;
                    switch (el.tagName) {
                        case 'LI':
                            if(arg.current == +el.innerHTML) return;
                            var list = el.parentNode.children;
                            for (var i = 0; i < list.length; i++) {
                                list[i].className = list[i] == el ? 'active' : '';
                            }
                            arg.current = +el.innerHTML;
                            if(callback) callback(arg.current);
                            break;
                        case 'SPAN':
                            switch (el.className) {
                                case 'first':
                                    if (start !== 0) {
                                        start = 0;
                                        build(start);
                                    }
                                    break;
                                case 'last':
                                    end = arg.items - arg.inrow;
                                    if (start != end) {
                                        start = end;
                                        build(start);
                                    }
                                    break;
                                case 'next':
                                    start += arg.step;
                                    if (start >= arg.items - arg.inrow) {
                                        start = arg.items - arg.inrow;
                                    }
                                    build(start);
                                    break;
                                case 'prev':
                                    if (start < arg.step) {
                                        start = 0;
                                    } else {
                                        start -= arg.step;
                                        if (start >= arg.items - arg.inrow) {
                                            start = arg.items - arg.inrow - arg.step;
                                        }
                                    }
                                    build(start);
                                    break;
                            }
                            break;
                    }
                };
            };
            window.Panel = Panel;
        }());
    </script> <!-- скрипт для пагинации-->

<script>
        $(function() {
            $(".search_button").click(function() {

                var searchString    = $("#search_box").val();
                var data1            = {
                    search: searchString,
                    page:  <?=$page?>
                };
                //if(searchString) {
                    $.ajax({
                        type: "POST",
                        url: "php/do_search.php",
                        data: data1,
                        success: function(html){
                            $("#goods").html(html);
                        }
                    });
                   // }
                $('input:checked').prop('checked', false);
                $('[name="min_price"]').val(0);
                $('[name="max_price"]').val(500);
                category_list ="{}";
                $( "#slider-range" ).slider({
                    // orientation: "vertical",
                    // step: 15,
                    range: true,
                    min: 0,
                    max: 500,
                    values: [0, 500]
                });
                return false;
            });
        });
    </script><!-- ajax запрос для поиска-->


<script src="js/mobile_menu.js"></script>
<script src="js/polzynok.js"></script>
<script src="js/scripts.js"></script>
</body>

</html>
