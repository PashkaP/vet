


    $('#carousel').carousel({
        interval: 6000,
        pause: 'hover',
        wrap: true

    });

//<!--скрипт для слайдера -->


var close = function() {
    var b = document.getElementsByClassName('mask-content')[0];
    $('.mask-content').click(function() {
        $('#menu_mobile').animate({
            left: '-285px'
        }, 200);

        $('body').animate({
            left: '0px'
        }, 200);
        b.style.display = 'none';
    });
}
$(document).ready(close);
//<!-- при нажатии на затемненную область меня уходыт влево-->



$(function(){
    $('.spoyler').click(function(event) {
        event.preventDefault();

        if ( $(this).next('div').is(':visible') ) {
            $(this).next('div').animate({height: 'hide'}, 500);
        } else {
            $('.spoyler-content').animate({height: 'hide'}, 500);
            $(this).next('div').animate({height: 'show'}, 500);
        }

    });
});
//<!--спойлер в товарах -->

var checklist = [];
var category_list ="{}";
jQuery(function ($) {
    $(document).on('change', '.checkbox', function () {
        var val = this.value|0; // to int

        if (this.checked) {
            checklist.push(val); // если в начало, то .ushift(val)

        } else {
            var idx = $.inArray(val, checklist);
            if( idx > -1 ){
                checklist.splice(idx, 1);
            }
        }
        category_list = JSON.stringify(checklist);
    });
});
//<!-- список отмеченых checkboxs-->


$(".button").click(function(){
    $(".modal-body").html("Loading...");
    var data = {
        id:$(this).attr("data-id")
    };
    $.ajax({
        type: "POST",
        data: data,
        url: "php/ajax.php",
        success: function(html){
            $(".modal-body").html(html);
        }
    });
});
//<!-- ajax запрос для модальных окон-->


var sorting_value;
function slctt(Val) {
    sorting_value = Val;
    sorting_value = parseInt(sorting_value,10);
}
//<!-- принимает число по которой сортировать товары-->


$(".filter_button").click(function(){
    var searchString    = $("#search_box").val();
    var data = {
        min_price: $('[name="min_price"]').val(),
        max_price: $('[name="max_price"]').val(),
        category_list: category_list,
        sorting_value: sorting_value,
        search: searchString
    };
    $.ajax({
        type: "POST",
        data: data,
        url: "php/ajax_goods.php",
        success: function(html){
            $("#goods").html(html);
        }
    });
});

// <!-- ajax запрос для товаров -->




$( ".target" ).change(function() {
    var searchString    = $("#search_box").val();
    var data = {
        min_price: $('[name="min_price"]').val(),
        max_price: $('[name="max_price"]').val(),
        category_list: category_list,
        sorting_value: sorting_value,
        search: searchString
    };
    $.ajax({
        type: "POST",
        data: data,
        url: "php/ajax_goods.php",
        success: function(html){
            $("#goods").html(html);
        }
    });
});
// ajax запрос для сортировкпи по...-->


function my_f1() {
    var object1 = document.getElementsByClassName('dropdown_block')[0];
    var angle1 = document.getElementById('dropdown_angle1');
    object1.style.display == 'none' ? object1.style.display = 'block' : object1.style.display = 'none';
    if (object1.style.display == 'none') {
        angle1.className = "fa fa-angle-down";
    } else {
        angle1.className = "fa fa-angle-up";
    }
}
function my_f2() {
    var object2 = document.getElementsByClassName('dropdown_block')[1];
    var angle2 = document.getElementById('dropdown_angle2');
    object2.style.display == 'block' ? object2.style.display = 'none' : object2.style.display = 'block';
    if (object2.style.display == 'block') {
        angle2.className = "fa fa-angle-up";
    } else {
        angle2.className = "fa fa-angle-down";
    }
}
function my_f3() {
    var object3 = document.getElementsByClassName('dropdown_block')[2];
    var angle3 = document.getElementById('dropdown_angle3');
    object3.style.display == 'block' ? object3.style.display = 'none' : object3.style.display = 'block';
    if (object3.style.display == 'block') {
        angle3.className = "fa fa-angle-up";
    } else {
        angle3.className = "fa fa-angle-down";
    }
}

//<!-- скрипты для dropdown-->