<div class="container-fluid bd-example-row">
    <div class="row">
        <div class="col-sm-6">
            <img src="<?php if(isset($www['photo'])) {
                print $www['photo'];}
            else {
                print "Ошибка";
            }?>" alt="milbemax">
            <p class="item-name"><?php if(isset($www['name'])) {
                    print $www['name'];}
                else {
                    print "Ошибка";
                }?></p>
            <p><span>ЦЕНА*: </span><?php if(isset($www['price'])) {
                    print $www['price'];}
                else {
                    print "Ошибка";
                }?> грн</p>
            <p class="description">*цену уточняйте у администратора клиники</p>
        </div>
        <div class="col-sm-6">
            <div class="panel-group" id="accordion">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                <span class="glyphicon glyphicon-minus"></span>
                                Форма выпуска
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <?php if(isset($www['form_of_issue'])) {
                                print $www['form_of_issue'];}
                            else {
                                print "Ошибка";
                            }?>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                <span class="glyphicon glyphicon-plus"></span>
                                Показания
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php if(isset($www['indications'])) {
                                print $www['indications'];}
                            else {
                                print "Ошибка";
                            }?>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                <span class="glyphicon glyphicon-plus"></span>
                                Противопоказания
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php if(isset($www['contraindications'])) {
                                print $www['contraindications'];}
                            else {
                                print "Ошибка";
                            }?>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                <span class="glyphicon glyphicon-plus"></span>
                                Побочные действия
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php if(isset($www['side_effects'])) {
                                print $www['side_effects'];}
                            else {
                                print "Ошибка";
                            }?>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                <span class="glyphicon glyphicon-plus"></span>
                                Способ применения
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php if(isset($www['mode_of_application'])) {
                                print $www['mode_of_application'];}
                            else {
                                print "Ошибка";
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $('.collapse').on('shown.bs.collapse', function(){
        $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hidden.bs.collapse', function(){
        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });
</script>